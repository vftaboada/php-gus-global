<?php

// mimic autoloading ...
include './base/BaseController.php';
include './base/Model.php';
include './base/View.php';
include './controller/ShopController.php';
include './controller/ProductController.php';
include './model/OpeningTimesModel.php';
include './model/ProductsModel.php';
include './view/OpeningTimesView.php';
include './view/ProductsView.php';
include './view/ContactUsView.php';

// connect to db as well maybe
$mysqliDb = new mysqli('0.0.0.0:3300', 'root', 'password', 'php_exam');