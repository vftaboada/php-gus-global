<?php

namespace Arden;

class ProductsModel extends Model
{
    public function __construct($db) {
        parent::__construct($db);
    }

    public function getData() {
        $sql = "SELECT * from products LIMIT 5";

        return $this->query($sql);
    }
}