<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include './bootstrap.php';
?>


<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <title>Shop</title>
        <meta name="description" content="Shop">
        <meta name="author" content="Arden University">
    </head>
    <body>
        <h1>Shop</h1>

        <div>
            <h2>Opening times</h2>

            <?php
            $controller = new Arden\ShopController(new Arden\OpeningTimesModel($mysqliDb));
            $openingTimesView = new Arden\OpeningTimesView();
            $openingTimesView->setData($controller->getModelData());

            echo $openingTimesView->render();
            ?>
        <div>
            <h2>Top Products</h2>
             
             <?php
                $productController = new Arden\ProductController(new Arden\ProductsModel($mysqliDb));
                $productView = new Arden\ProductsView();
                $productView->setData($productController->getModelData());
                $productView->render();
             ?>
        </div>

        <div>
            <h2> Contact Us </h2>
            <?php
                // $contactUsController = new Arden\ContactUsController();
                $contactUsView = new Arden\ContactUsView();
                $contactUsView->render();
            ?>
        </div>
    </body>
</html>
