<?php

namespace Arden;

class ContactUsView extends View
{
    public function __construct($data = null)
    {
        if($data) {
            $this->data = $data;
        }
    }

    public function render() {
       return include 'form.php';
    }
}