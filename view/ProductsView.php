<?php

namespace Arden;

class ProductsView extends View
{
    public function __construct($data = null)
    {
        if($data) {
            $this->data = $data;
        }
    }

    public function render() {
        $products = $this->data;
        return include('html/_product_list.php');
    }
}