<table>
    <tr>
        <?php foreach($days as $key => $day): ?>
            <th width="20%"><?php echo $day?></th>
            <?php
                if (array_key_exists($day, $openingHours)) {
                    $hours[] = $openingHours[$day];
                } else {
                    $hours[] = '-';
                }
            ?>
        <?php endforeach;?>
    <tr>
    <tr>
        <?php foreach($hours as $key => $hour):?>
            <td style="text-align:center"><?php echo $hour?></td>
        <?php endforeach?>
    <tr>
</table>