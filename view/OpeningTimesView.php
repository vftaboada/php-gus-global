<?php

namespace Arden;

class OpeningTimesView extends View
{
    public function __construct($data = null)
    {
        if($data) {
            $this->data = $data;
        }
    }

    public function render() {
        $header = '';
        $entry = '';

        $days = $this->data['days'];
        $openingHours = $this->data['opening_hours'];
        include 'html/_opening_hours.php';
        // foreach($this->data['days'] as $key => $value) {
        //     // header
        //     $header .= "<th width='20%' style='text-align:center'>$value</th>";
        //     // entry

        //     $hours = '';
        //     if (array_key_exists($value, $this->data['opening_hours'])) {
        //         $hours = $this->data['opening_hours'][$value];
        //     }
        //     $entry .= "<td style='text-align:center'>$hours</td>";
        // }
        

        // echo '<table>';
        // echo '<tr>';
        // echo $header;
        // echo '</tr>';
        // echo '<tr>';
        // echo $entry;
        // echo '</tr>';
    
        // echo '</table>';
    }
}