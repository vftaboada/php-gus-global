<?php

namespace Arden;

abstract class View
{
    protected $data;

    public function setData($data) {
        $this->data = $data;
    }

    public function getData() {
        return $this->data;
    }

    abstract public function render();

}