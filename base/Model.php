<?php

namespace Arden;

class Model
{
    protected $data;
    protected $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function query($sql) {
        $result = $this->db->query($sql);
        if (!is_object($result)) {
            return $result;
        }
        $data = [];
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }
        return $data;
    }

    public function getData() {
        return $this->data;
    }
}