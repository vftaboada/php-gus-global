<?php

namespace Arden;

abstract class BaseController
{
    protected $modelData;

    public function getModelData() {
        return $this->modelData;
    }
}