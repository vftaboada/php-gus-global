<?php

namespace Arden;

class ShopController extends BaseController
{
    public function __construct($model)
    {
        $this->modelData = $model->getData();
    }
}