<?php

namespace Arden;

class ProductController extends BaseController
{
    public function __construct($model)
    {
        $this->modelData = $model->getData();
    }
}